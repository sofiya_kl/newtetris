﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CubePart::SetColor(System.Single,System.Single,System.Single)
extern void CubePart_SetColor_m869AEBB0729EDD89B8F0C68F3B1A06E22FB1E5A0 ();
// 0x00000002 System.Void CubePart::Start()
extern void CubePart_Start_mD630C38A9B0082D8DE33B4547AC80DD35E9175A7 ();
// 0x00000003 System.Void CubePart::Update()
extern void CubePart_Update_m978251655AFD92EC9CDBDBA8989511890CD18E82 ();
// 0x00000004 System.Void CubePart::.ctor()
extern void CubePart__ctor_m7B9178B9117D75F0EA231561F48FA38D49614224 ();
// 0x00000005 System.Void RotateCube::Update()
extern void RotateCube_Update_m03CA8D6E1D01DA5DED657978C04D8E07CC80C061 ();
// 0x00000006 System.Void RotateCube::.ctor()
extern void RotateCube__ctor_m9A2276294CF5FCC27DF486D5E2A8632DE912C6FC ();
// 0x00000007 System.Void TapToPlace::Start()
extern void TapToPlace_Start_m43B0028354F221121438A54F52FC2D25D350E8B5 ();
// 0x00000008 System.Void TapToPlace::Awake()
extern void TapToPlace_Awake_m3DD729720B8C1A6AA017374699DD0564B7BF7E8B ();
// 0x00000009 System.Void TapToPlace::Update()
extern void TapToPlace_Update_mE6AF20E9CC5137BB575093ABC52D42E6490E160A ();
// 0x0000000A System.Boolean TapToPlace::TryGetTouchPosition(UnityEngine.Vector2&)
extern void TapToPlace_TryGetTouchPosition_m6B7311C17E43A22EB2B6606BF6E27B88E6FC9CDA ();
// 0x0000000B System.Void TapToPlace::.ctor()
extern void TapToPlace__ctor_m0E3BD5F22A552D0B2B61B45A07F54343F483D643 ();
// 0x0000000C System.Void TapToPlace::.cctor()
extern void TapToPlace__cctor_m148B83B7E289AD9DAD637DA5D179B933FB8A9CB7 ();
// 0x0000000D System.Void cube::Start()
extern void cube_Start_mC100D10F9DBF4879FFB4B7DB80A76D074C912FBC ();
// 0x0000000E System.Void cube::Update()
extern void cube_Update_m6FA78B74DB24BED0F84200A644A553C9158EBA55 ();
// 0x0000000F System.Void cube::CreateCube()
extern void cube_CreateCube_mD2CA1BAB5DE566EA727A5E35F6E4AEE439141A3C ();
// 0x00000010 System.Void cube::.ctor()
extern void cube__ctor_mED709A94FBEF60D2E0E4E6A86C52AD1B41F109EC ();
static Il2CppMethodPointer s_methodPointers[16] = 
{
	CubePart_SetColor_m869AEBB0729EDD89B8F0C68F3B1A06E22FB1E5A0,
	CubePart_Start_mD630C38A9B0082D8DE33B4547AC80DD35E9175A7,
	CubePart_Update_m978251655AFD92EC9CDBDBA8989511890CD18E82,
	CubePart__ctor_m7B9178B9117D75F0EA231561F48FA38D49614224,
	RotateCube_Update_m03CA8D6E1D01DA5DED657978C04D8E07CC80C061,
	RotateCube__ctor_m9A2276294CF5FCC27DF486D5E2A8632DE912C6FC,
	TapToPlace_Start_m43B0028354F221121438A54F52FC2D25D350E8B5,
	TapToPlace_Awake_m3DD729720B8C1A6AA017374699DD0564B7BF7E8B,
	TapToPlace_Update_mE6AF20E9CC5137BB575093ABC52D42E6490E160A,
	TapToPlace_TryGetTouchPosition_m6B7311C17E43A22EB2B6606BF6E27B88E6FC9CDA,
	TapToPlace__ctor_m0E3BD5F22A552D0B2B61B45A07F54343F483D643,
	TapToPlace__cctor_m148B83B7E289AD9DAD637DA5D179B933FB8A9CB7,
	cube_Start_mC100D10F9DBF4879FFB4B7DB80A76D074C912FBC,
	cube_Update_m6FA78B74DB24BED0F84200A644A553C9158EBA55,
	cube_CreateCube_mD2CA1BAB5DE566EA727A5E35F6E4AEE439141A3C,
	cube__ctor_mED709A94FBEF60D2E0E4E6A86C52AD1B41F109EC,
};
static const int32_t s_InvokerIndices[16] = 
{
	1134,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	780,
	23,
	3,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	16,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
