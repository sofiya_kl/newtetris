﻿using UnityEngine;

public class RotateCube : MonoBehaviour
{
    public Transform AnyCube;
    public float SpeedRotateX = 5;
    public float SpeedRotateY = 5;

    void Update()
    {
        if (!Input.GetMouseButton(0))
            return;

        float rotX = Input.GetAxis("Mouse X") * SpeedRotateX * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * SpeedRotateY * Mathf.Deg2Rad;

        if (Mathf.Abs(rotX) > Mathf.Abs(rotY))
            AnyCube.RotateAroundLocal(AnyCube.up, -rotX);
        else
        {
            var prev = AnyCube.rotation;
            AnyCube.RotateAround(Camera.main.transform.right, rotY);
            if (Vector3.Dot(AnyCube.up, Camera.main.transform.up) < 0.5f)
                AnyCube.rotation = prev;
        }
    }
}