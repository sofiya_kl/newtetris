﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cube : MonoBehaviour
{
    public GameObject CubePart;
    Transform CubeTransf;
    List<GameObject> AllPart = new List<GameObject>();
    GameObject CenterPart;



    /*List<GameObject> UpPart
	{
		get
		{
            return AllPart.FindAll(x => MathF.Round(x.transfom.localPosition.y) == 0);
		}
	}
    List<GameObject> DownPart
	{

	}
    List<GameObject> FrontPart
	{

	}
    List<GameObject> BackPart
	{

	}
    List<GameObject> LeftPart
	{

	}
    List<GameObject> RightPart
	{

	} 

    Vector3[] RotationVec =
    {
        new Vector3 (0,1,0),new Vector3 (0,-1,0),new Vector3 (0,0,-1),
        new Vector3 (0,0,1),new Vector3 (1,0,0),new Vector3 (-1,0,0)
    };*/



    // Start is called before the first frame update
    void Start()
    {
        CreateCube();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void CreateCube()
	{
        
        for (float x=-0.02f; x<=0.02; x+=0.02f)
            for (float  y=-0.02f; y<=0.02; y+=0.02f)
                for (float   z=-0.02f; z<=0.02; z+=0.0f)
				{
                    GameObject go = Instantiate(CubePart, CubeTransf, false);
                    go.transform.localPosition = new  Vector3(-x, -y, z);
                    go.GetComponent<CubePart>().SetColor(-x, -y, z);
                    AllPart.Add(go);
                }
        CenterPart = AllPart[13];
    }


    /*IEnumerator Shuffle()
	{
        canShaffle = false;
        for (int moveCount = Random.Range(15, 30); moveCount >=0; moveCount--)
		{
            int side = Random.range(0, 6);
            List<GameObject> sidePart = new List<GameObject>();
			switch (side)
			{
                case 0:sidePart = UpPart; break ;
                case 1:sidePart = DownPart; break;
                case 2:sidePart = LeftPart; break;
                case 3:sidePart = RightPart; break;
                case 4:sidePart = FrontPart; break;
                case 5:sidePart = BackPart; break;

			}
            StartCoroutine(Rotate(sidePart, RotationVec [side]));
            yield return new WaitForSeconds(.3f);
		} 
	}*/
}
