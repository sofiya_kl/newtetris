﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class TapToPlace: MonoBehaviour
{
    public GameObject gameObjectToInst;
 //   public GameObject CubePieces;

 //   Transform CubeTr;

    private GameObject go;
    private GameObject spawnobject;
    private ARRaycastManager _aRRaycastManager;
    private Vector2 touchposition;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
  //  static public ARPlaneManager _aRPlaneManager;

    void Start()
    {
        
    }

    private void Awake()
    {   
        _aRRaycastManager = GetComponent<ARRaycastManager>();
    }

    void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchposition))
            return;
        if (_aRRaycastManager.Raycast(touchposition, hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = hits[0].pose;

            if (spawnobject == null)
            {

                for (float x = 0; x <= 0.1025f; x += 0.1025f)
                    for (float y = 0; y <= 0.1025f; y += 0.1025f)
                        for (float z = 0; z <= 0.1025f; z += 0.1025f)
                        {
                            spawnobject = Instantiate(gameObjectToInst, hitPose.position, Quaternion.identity);
                            spawnobject.transform.position = new Vector3(-x, -y, z);

                        }
                gameObjectToInst.SetActive(false);
            }
           
        }
    }

    bool TryGetTouchPosition (out Vector2 touchPosition)
    {
         if (Input.touchCount == 1)
        {
            touchPosition = Input.GetTouch(index: 0).position;
            return true;
        }
        touchPosition = default;
        return false;
    }
    
}
