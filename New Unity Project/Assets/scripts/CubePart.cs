﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePart : MonoBehaviour
{
    public GameObject UpPart, DownPart, LeftPart,
           RightPart, FrontPart, BackPart;


    public void SetColor (float x, float y, float z)
	{
        if (y == 0.02f) UpPart.SetActive(true);
        else if (y == -0.02f) DownPart.SetActive(true);
        if (z == 0.02f) RightPart.SetActive(true);
        else if (z == -0.02f) LeftPart.SetActive(true);
        if (x == 0.02f) FrontPart.SetActive(true);
        else if (x == -0.02f) BackPart.SetActive(true);
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
